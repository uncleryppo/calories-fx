package de.ithappens.calories;

import de.ithappens.commons.usercontext.EnvironmentPreference;

/**
 * Copyright: 2018
 * Organisation: IT-Happens.de
 * @author riddler
 */
public class CaloriesPrefs extends EnvironmentPreference {

    private final static String CALORIES_DB_LOCATION = "calories-database-location";

    @Override
    public CONTEXT getContext() {
        return CONTEXT.USER;
    }

    public String getDatabaseLocation() {
        return getString(CALORIES_DB_LOCATION, "./calories_test.db");
    }

    public void setDatabaseLocation(String location) {
        setString(CALORIES_DB_LOCATION, location);
    }

}
