package de.ithappens.calories;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Copyright: 2018
 * Organisation: IT-Happens.de
 * @author riddler
 */
public class UiHelper {
    
    private final static Logger LOG = LogManager.getLogger(UiHelper.class);
    private final static SimpleDateFormat DF = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss.SSS");

    private final static String BUNDLE_NAME = "de.ithappens.calories.calories";
    private final static ResourceBundle RB = ResourceBundle.getBundle(BUNDLE_NAME);

    private final static String FXML_NAME = "de/ithappens/calories/calories-main.fxml";
    private final static String APP_ICON = "de/ithappens/calories/calories.png";

    public final static String APPLICATION_VERSION = get("APPLICATION_VERSION");
    public final static String APPLICATION_NAME = get("APPLICATION_NAME");
    
    private static String get(String key) {
        String value = null;
        try {
            value = RB.getString(key);
        } catch (Exception ex) {
            ex.printStackTrace();
            value = "<TEXT_ERROR>";
        } finally {
            if (value == null || value.equals("")) {
                value = key;
            }
            return StringEscapeUtils.unescapeJava(value);
        }
    }

    public static FXMLLoader getFXMLLoader() throws IOException {
        ClassLoader classLoader = UiHelper.class.getClassLoader();
        String bundleFilename = StringUtils.replace(BUNDLE_NAME, ".", "/") + ".properties";
        System.out.println("bundleFilename: " + bundleFilename);
        return new FXMLLoader(
                classLoader.getResource(FXML_NAME),
                new PropertyResourceBundle(classLoader.getResource(bundleFilename).openStream()));
    }

    public static Image getAppIcon() throws IOException {
        ClassLoader classLoader = UiHelper.class.getClassLoader();
        return new Image(classLoader.getResource(APP_ICON).openStream());
    }

    public static void setCursorWait(Stage stage, boolean wait) {
        stage.getScene().setCursor(wait ? Cursor.WAIT : Cursor.DEFAULT);
    }

}
