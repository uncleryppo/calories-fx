package de.ithappens.calories;

import de.ithappens.calories.database.CaloriesDatabase;
import de.ithappens.commons.usercontext.UserContext;

import java.sql.SQLException;

public class SourceController {

    private final static CaloriesPrefs PREFS = UserContext.getInstance().getEnvironment(CaloriesPrefs.class);

    public static CaloriesDatabase caloriesDatabase() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        return new CaloriesDatabase(PREFS.getDatabaseLocation());
    }

}
