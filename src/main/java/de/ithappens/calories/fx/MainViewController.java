package de.ithappens.calories.fx;

import de.ithappens.calories.SourceController;
import de.ithappens.calories.database.CaloriesDatabase;
import de.ithappens.calories.database.model.generated.tables.pojos.Consumption;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Copyright: 2018
 * Organisation: IT-Happens.de
 * @author riddler
 */
public class MainViewController {

    @FXML
    TableView<Consumption> consumptions_tbv;
    @FXML
    TableColumn<Consumption, Timestamp> consumptionDate_col;
    @FXML
    TableColumn<Consumption, String> consumptionId_col, consumptionIngredients_col;
    @FXML
    TableColumn<Consumption, BigDecimal> consumptionKcal_col;

    private CaloriesDatabase DB;

    private CaloriesDatabase DB() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        return DB != null
                ? DB
                : (DB = SourceController.caloriesDatabase());
    }

    @FXML
    public void actionDownloadConsumptions() {
        try {
            consumptions_tbv.getItems().clear();
            consumptions_tbv.getItems().addAll(DB().ConsumptionEngine().findAll());
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println("so long");
    }

    @FXML
    public void actionUploadConsumptions() {
        try {
            DB().ConsumptionEngine().updateOrInsert(consumptions_tbv.getItems());
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println("mach et");
    }

    @FXML
    public void actionAddConsumption() {
        System.out.println("boo");
        TableHelper.addConsumption(consumptions_tbv);
        for (Consumption c : consumptions_tbv.getItems()) {
            System.out.println("c-ingredients: " + c.getIngredients());
            System.out.println("c-kcal: " + c.getKcal());
            System.out.println("c-consumeDate: " + c.getConsumedate());
        }
    }

    public void initUi() {
        TableHelper.prepareConsumptionTable(
                consumptionDate_col, consumptionId_col, consumptionIngredients_col, consumptionKcal_col);

    }
}

