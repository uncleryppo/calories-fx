package de.ithappens.calories.fx;

import de.ithappens.calories.database.model.generated.tables.pojos.Consumption;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class TableHelper {

    public static void prepareConsumptionTable(
            TableColumn<Consumption, Timestamp> consumptionDate_col,
            TableColumn<Consumption, String> consumptionId_col,
            TableColumn<Consumption, String> consumptionIngredients_col,
            TableColumn<Consumption, BigDecimal> consumptionKcal_col
    ) {
        consumptionDate_col.setCellValueFactory(
                cellData -> cellData.getValue() != null
                        ? new SimpleObjectProperty<>(cellData.getValue().getConsumedate())
                        : null);
        consumptionDate_col.setCellFactory(col -> new EditableDateTableCell<>());
        consumptionDate_col.setOnEditCommit((TableColumn.CellEditEvent<Consumption, Timestamp> event) -> {
            getConsumptionFromEventPosition(event).setConsumedate(event.getNewValue());
        });

        consumptionId_col.setCellValueFactory(
                cellData -> cellData.getValue() != null
                        ? new SimpleStringProperty(cellData.getValue().getId())
                        : null);

        consumptionIngredients_col.setCellValueFactory(
                cellData -> cellData.getValue() != null
                        ? new SimpleStringProperty(cellData.getValue().getIngredients())
                        : null);

        consumptionIngredients_col.setCellFactory(TextFieldTableCell.<Consumption>forTableColumn());
        consumptionIngredients_col.setOnEditCommit((TableColumn.CellEditEvent<Consumption, String> event) -> {
            getConsumptionFromEventPosition(event).setIngredients(event.getNewValue());
        });

        consumptionKcal_col.setCellValueFactory(
                cellData -> cellData.getValue() != null
                        ? new SimpleObjectProperty<>(cellData.getValue().getKcal())
                        : null);
        consumptionKcal_col.setCellFactory(col -> new EditableBigDecimalTableCell<>());
        consumptionKcal_col.setOnEditCommit((TableColumn.CellEditEvent<Consumption, BigDecimal> event) -> {
            getConsumptionFromEventPosition(event).setKcal(event.getNewValue());
        });
    }


    public static Consumption getConsumptionFromEventPosition(TableColumn.CellEditEvent<Consumption, ?> event) {
        TablePosition<Consumption, ?> pos = event.getTablePosition();
        return event.getTableView().getItems().get(pos.getRow());
    }

    public static void addConsumption(TableView<Consumption> consumptions_tbv) {
        Consumption newConsumption = new Consumption();
        consumptions_tbv.getItems().add(newConsumption);
        consumptions_tbv.getSelectionModel().select(newConsumption);

    }
}
