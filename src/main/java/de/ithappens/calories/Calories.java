package de.ithappens.calories;

import com.jpro.webapi.JProApplication;
import de.ithappens.calories.fx.MainViewController;
import de.ithappens.commons.usercontext.UserContext;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Copyright: 2018
 * Organisation: IT-Happens.de
 * @author riddler
 */
public class Calories extends JProApplication {
    
    Logger log = LogManager.getLogger(getClass());
    
    public static void main(String args[]) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            //prepare
            UserContext.getInstance().addEnvironment(new CaloriesPrefs());
            String appTitleAndVersion = UiHelper.APPLICATION_NAME
                    + " v" + UiHelper.APPLICATION_VERSION;
            log.debug("Run " + appTitleAndVersion);
            //view
            setUserAgentStylesheet(STYLESHEET_MODENA);
            FXMLLoader guiLoader = UiHelper.getFXMLLoader();
            Parent root = (Parent) guiLoader.load();
            MainViewController controller = guiLoader.getController();
            controller.initUi();
            Scene scene = new Scene(root);
            primaryStage.setOnCloseRequest((event) -> {
                log.debug("Application windows closed -> Quit application");
                System.exit(0);
            });
            primaryStage.initStyle(StageStyle.DECORATED);
            primaryStage.getIcons().add(UiHelper.getAppIcon());
            primaryStage.setTitle(appTitleAndVersion);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch(Exception ex) {
            ex.printStackTrace();
            log.error(ex);
            System.exit(1);
        }
    }

}
